# ansible-role-k8s

Ansible-Role to install a K8s Cluster

## Info: if you're planning to install a Kubernetes Cluster with Raspberry Pi's, choose "Ubuntu Server (amd64)" instead of "Raspberry Pi OS", because due to it's compatibility with older Raspberry Pi Versions, RPi OS is using armv7 on Raspberry Pi 4, which is not supported by a lot of Docker Container Images.

## Vars:
name		| type	  | description
----------------|---------|------------------------
apisrv_adv_addr	| string  | ip address of apisrv (only control plane)
k8smaster	| bool	  | specifies, if node is a control plane
k8sStandalone	| bool	  | specifies, if pods can run on control plane

